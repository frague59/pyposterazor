"""
Gui for :mod:`posterazor` application
"""
import logging
from gettext import gettext as _
from pathlib import Path
from typing import Optional, Tuple
from PIL import Image
import wx
from wx.lib.statbmp import GenStaticBitmap
from .helpers import ImageBox, MarginsBoxSizer, PaperSizeBoxSizer, OrientationBoxSizer, BORDER_WIDTH, ImagePropertiesBox
from posterazor import PosteRazor, enums, helpers

logger = logging.getLogger(__name__)
settings = helpers.Settings()


# noinspection DuplicatedCode
class AppWindow(wx.App):
    """
    Main application window
    """

    _image_file: Optional[wx.Image] = None
    _image_path: Optional[Path] = None

    _posterazor: Optional[PosteRazor] = None

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.main_frame = wx.Frame(
            None,
            title=_("PosteRazor"),
            style=wx.MAXIMIZE_BOX | wx.RESIZE_BORDER | wx.SYSTEM_MENU | wx.CAPTION | wx.CLOSE_BOX,
            size=(640, 480),
        )
        # Initialize the menu
        self._add_menu(parent=self.main_frame)
        self.left, self.right = self._add_hz_box(parent=self.main_frame)

        # Initialize the export parameters
        self.w_paper_size = PaperSizeBoxSizer(orient=wx.VERTICAL, parent=self.main_frame, label=_("Paper size"))
        self.left.Add(self.w_paper_size, flag=wx.ALL | wx.EXPAND | wx.ALIGN_TOP, border=BORDER_WIDTH)

        self.w_orientation = OrientationBoxSizer(orient=wx.VERTICAL, parent=self.main_frame, label=_("Orientation"))
        self.left.Add(self.w_orientation, flag=wx.ALL | wx.EXPAND | wx.ALIGN_TOP, border=BORDER_WIDTH)

        self.w_margins = MarginsBoxSizer(orient=wx.VERTICAL, parent=self.main_frame, label=_("Margins (mm)"))
        self.left.Add(self.w_margins, flag=wx.ALL | wx.EXPAND | wx.ALIGN_TOP, border=BORDER_WIDTH)

        self.w_button = wx.Button(self.main_frame, id=wx.ID_ANY, label=_("Crop"))
        self.left.Add(self.w_button, flag=wx.ALL | wx.EXPAND | wx.ALIGN_TOP, border=BORDER_WIDTH)
        self.w_button.Bind(wx.EVT_BUTTON, self.on_crop_click)
        self.w_image = ImageBox(
            parent=self.main_frame,
            ID=wx.ID_ANY,
            bitmap=wx.EmptyBitmap(2018, 2048),
            sizer=self.right
        )
        self.right.Add(self.w_image, proportion=2, flag=wx.ALIGN_TOP)

        self.w_properties = ImagePropertiesBox(parent=self.main_frame)
        self.right.Add(self.w_properties, flag=wx.ALL, border=BORDER_WIDTH)
        self.main_frame.Bind(wx.EVT_SIZE, self.w_image.on_resize)
        self.main_frame.Show()

    # region Widgets
    def _add_menu(self, parent):
        logger.debug(f"AppWindow::_add_menu()")

        # Create the file menu
        file_menu = wx.Menu()
        file_menu.Append(wx.ID_OPEN)
        file_menu.AppendSeparator()
        file_menu.Append(wx.ID_EXIT)

        parent.Bind(wx.EVT_MENU, self.on_open, id=wx.ID_OPEN)

        # Create the help menu
        help_menu = wx.Menu()
        help_menu.Append(wx.ID_HELP)
        help_menu.AppendSeparator()
        help_menu.Append(wx.ID_ABOUT)
        parent.Bind(wx.EVT_MENU, self.on_help, id=wx.ID_HELP)
        parent.Bind(wx.EVT_MENU, self.on_about, id=wx.ID_ABOUT)

        # Create the menu bar
        menu_bar = wx.MenuBar()
        menu_bar.Append(file_menu, _("&File"))
        menu_bar.Append(help_menu, _("&Help"))

        parent.SetMenuBar(menuBar=menu_bar)

    def _add_hz_box(self, parent) -> Tuple[wx.BoxSizer, wx.BoxSizer]:  # noqa
        h_box = wx.BoxSizer(orient=wx.HORIZONTAL)
        left = wx.BoxSizer(orient=wx.VERTICAL)
        right = wx.BoxSizer(orient=wx.VERTICAL)
        h_box.Add(left, proportion=1, flag=wx.ALL | wx.EXPAND, border=BORDER_WIDTH)
        h_box.Add(right, proportion=2, flag=wx.ALL | wx.EXPAND, border=BORDER_WIDTH)
        parent.SetSizer(h_box)
        return left, right
    # endregion Widgets

    # region Utilities
    def start(self):
        self.MainLoop()

    # endregion Utilities

    # region Commands
    def on_open(self, event) -> None:
        logger.debug(f"AppWindow::on_open({event})")
        dialog = wx.FileDialog(
            parent=self.main_frame,
            message=_("Select an image file"),
            defaultDir="$HOME",
            wildcard="PNG (*.png)|*.png|JPEG (*.jpg)|*.jpg",
        )
        dialog.ShowModal()
        logger.debug(f"AppWindow::on_open() file = {dialog.GetPath()}")
        self._image_path = Path(dialog.GetPath())
        self._image_file = wx.Image(str(self._image_path), wx.BITMAP_TYPE_ANY)
        pil_image: Image.Image = Image.open(self._image_path)
        self.w_properties.image_width = pil_image.width
        self.w_properties.image_height = pil_image.height
        self.w_properties.dpi = pil_image.info.get("dpi", (96, 96))
        self.w_image.image = pil_image
        dialog.Destroy()

    def on_crop_click(self, event):
        logger.debug(f"AppWindow::on_crop_click()")
        self._posterazor = PosteRazor(
            input_file=self.w_image.image,
            paper_size=self.w_paper_size.paper_size,
            orientation=self.w_orientation.orientation,
        )
        self.w_image.image = self._posterazor.get_preview(
            paper_size=self.w_paper_size.paper_size,
            orientation=self.w_orientation.orientation,
            margins=self.w_margins.margins
        )

    def on_help(self, event) -> None:
        logger.debug(f"AppWindow::on_help()")

    def on_about(self, event) -> None:
        logger.debug(f"AppWindow::on_about()")

    # endregion Commands

