"""
Generic helper class and additional panels
"""
import logging
from collections import namedtuple
from typing import Optional, Tuple

import wx
from gettext import gettext as _

from PIL.Image import Image
from wx.lib.statbmp import GenStaticBitmap

from posterazor import helpers, enums
from posterazor.helpers import px_to_mm

logger = logging.getLogger(__name__)
settings = helpers.Settings()

BORDER_WIDTH = 5
SIZER_FIELD_FLAG = wx.LEFT | wx.RIGHT | wx.EXPAND | wx.ALIGN_TOP


class ImageBox(GenStaticBitmap):
    _image: Optional[Image] = None
    _wx_image: Optional[wx.Image] = None

    # region Properties
    @property
    def image(self):
        return self._image

    def __init__(self, *args, **kwargs):
        self.sizer: wx.BoxSizer = kwargs.pop("sizer", None)
        super().__init__(*args, **kwargs)

    @image.setter
    def image(self, value):
        if value is None:
            return
        self.SetBitmap(wx.EmptyBitmap(2048, 2048))
        self._image = value
        image_width, image_height = self._image.size
        widget_width, widget_height = self.sizer.GetSize()
        size = self.get_best_image_size(image_width, image_height, widget_width, widget_height)
        display_image = self._image.resize(size)
        self._wx_image = wx.Image(size)
        self._wx_image.SetData(display_image.convert("RGB").tobytes())
        self.SetBitmap(self._wx_image.ConvertToBitmap())
        self.sizer.Layout()

    # endregion Properties

    @staticmethod
    def get_best_image_size(image_width, image_height, widget_width, widget_height) -> wx.Size:
        if image_width < image_height:
            height_ratio = widget_height / image_height
            width = round(image_width * height_ratio)
            height = round(image_height * height_ratio)
        else:
            width_ratio = widget_width / image_width
            width = round(image_width * width_ratio)
            height = round(image_height * width_ratio)

        output = wx.Size(width=width, height=height)
        logger.debug(f"AppWindow::get_size_pos() output = {output}")
        return output

    def on_resize(self, event) -> None:
        event.Skip()
        logger.debug(f"ImageBox::on_resize({event})")
        self.image = self.image


# noinspection DuplicatedCode
class MarginsBoxSizer(wx.StaticBoxSizer):
    margin_left: int = int(settings["margin_left"])
    margin_top: int = int(settings["margin_top"])
    margin_right: int = int(settings["margin_right"])
    margin_bottom: int = int(settings["margin_bottom"])

    @property
    def margins(self) -> enums.Margins:
        return enums.Margins(left=self.margin_left, top=self.margin_top, right=self.margin_right, bottom=self.margin_bottom)

    @margins.setter
    def margins(self, value: enums.Margins):
        self.margin_left = value.left
        self.margin_top = value.top
        self.margin_right = value.right
        self.margin_bottom = value.bottom

    def __init__(self, *args, **kwargs):
        self.parent = kwargs.get("parent")
        super().__init__(*args, **kwargs)
        self.s_margin_left_label = wx.StaticText(self.parent, id=wx.ID_ANY, label=_("Left Margin"), style=wx.ALIGN_LEFT)
        self.s_margin_left = wx.SpinCtrl(
            self.parent,
            id=wx.ID_ANY,
            initial=int(settings["margin_left"]),
            min=0,
            max=25,
            style=wx.LEFT | wx.SP_ARROW_KEYS,
        )
        self.s_margin_top_label = wx.StaticText(self.parent, id=wx.ID_ANY, label=_("Top margin"), style=wx.ALIGN_LEFT)
        self.s_margin_top = wx.SpinCtrl(
            self.parent,
            id=wx.ID_ANY,
            initial=int(settings["margin_top"]),
            min=0,
            max=25,
            style=wx.LEFT | wx.SP_ARROW_KEYS,
        )
        self.s_margin_right_label = wx.StaticText(
            self.parent, id=wx.ID_ANY, label=_("Right margin"), style=wx.ALIGN_LEFT
        )
        self.s_margin_right = wx.SpinCtrl(
            self.parent,
            id=wx.ID_ANY,
            initial=int(settings["margin_right"]),
            min=0,
            max=25,
            style=wx.LEFT | wx.SP_ARROW_KEYS,
        )
        self.s_margin_bottom_label = wx.StaticText(
            self.parent, id=wx.ID_ANY, label=_("Bottom margin"), style=wx.ALIGN_LEFT
        )
        self.s_margin_bottom = wx.SpinCtrl(
            self.parent,
            id=wx.ID_ANY,
            initial=int(settings["margin_bottom"]),
            min=0,
            max=25,
            style=wx.LEFT | wx.SP_ARROW_KEYS,
        )
        # Bind events
        self.s_margin_left.Bind(wx.EVT_SPINCTRL, handler=self.on_margin_change)
        self.s_margin_right.Bind(wx.EVT_SPINCTRL, handler=self.on_margin_change)
        self.s_margin_top.Bind(wx.EVT_SPINCTRL, handler=self.on_margin_change)
        self.s_margin_bottom.Bind(wx.EVT_SPINCTRL, handler=self.on_margin_change)

        self.Add(self.s_margin_left_label, flag=SIZER_FIELD_FLAG, border=BORDER_WIDTH)
        self.Add(self.s_margin_left, flag=SIZER_FIELD_FLAG, border=BORDER_WIDTH)
        self.Add(self.s_margin_top_label, flag=SIZER_FIELD_FLAG, border=BORDER_WIDTH)
        self.Add(self.s_margin_top, flag=SIZER_FIELD_FLAG, border=BORDER_WIDTH)
        self.Add(self.s_margin_right_label, flag=SIZER_FIELD_FLAG, border=BORDER_WIDTH)
        self.Add(self.s_margin_right, flag=SIZER_FIELD_FLAG, border=BORDER_WIDTH)
        self.Add(self.s_margin_bottom_label, flag=SIZER_FIELD_FLAG, border=BORDER_WIDTH)
        self.Add(self.s_margin_bottom, flag=SIZER_FIELD_FLAG, border=BORDER_WIDTH)

    def on_margin_change(self, event: wx.SpinEvent) -> None:
        source = event.GetEventObject()
        if source is self.s_margin_left:
            self.margin_left = source.GetValue()
            logger.debug(f"AppWindow::on_margin_change() self._margin_left = {self.margin_left}")
            return
        if source is self.s_margin_top:
            self.margin_top = source.GetValue()
            logger.debug(f"AppWindow::on_margin_change() self._margin_top = {self.margin_top}")
            return
        if source is self.s_margin_right:
            self.margin_right = source.GetValue()
            logger.debug(f"AppWindow::on_margin_change() self._margin_right = {self.margin_right}")
            return
        if source is self.s_margin_bottom:
            self.margin_bottom = source.GetValue()
            logger.debug(f"AppWindow::on_margin_change() self._margin_bottom = {self.margin_bottom}")
            return


class PaperSizeBoxSizer(wx.StaticBoxSizer):
    paper_size: enums.PaperSize = enums.PaperSize[settings["paper_size"]]

    def __init__(self, *args, **kwargs):
        self.parent = kwargs.get("parent")
        super().__init__(*args, **kwargs)
        self.w_paper_size = wx.Choice(
            self.parent,
            id=wx.ID_ANY,
            choices=[name for name, value in enums.PaperSize.__members__.items()],
        )
        self.w_paper_size.SetSelection(1)
        self.w_paper_size.Bind(wx.EVT_CHOICE, self.on_ps_choice)
        self.Add(self.w_paper_size, flag=SIZER_FIELD_FLAG, border=BORDER_WIDTH)

    def on_ps_choice(self, event: wx.Event) -> None:
        logger.debug(f"PaperSizeBoxSizer::on_ps_choice({event})")
        orient = event.GetEventObject()
        self.paper_size = enums.PaperSize[orient.GetStringSelection()]


class OrientationBoxSizer(wx.StaticBoxSizer):
    orientation: enums.Orientation = enums.Orientation[settings["orientation"]]

    def __init__(self, *args, **kwargs):
        self.parent = kwargs.get("parent")
        super().__init__(*args, **kwargs)
        self.orient = wx.Choice(
            self.parent,
            id=wx.ID_ANY,
            choices=[name for name, value in enums.Orientation.__members__.items()],
        )
        self.orient.SetSelection(1)
        self.orient.Bind(wx.EVT_CHOICE, self.on_orientation_choice)
        self.Add(self.orient, flag=SIZER_FIELD_FLAG, border=BORDER_WIDTH)

    def on_orientation_choice(self, event: wx.Event) -> None:
        logger.debug(f"AppWindow::on_orientation_choice({event})")
        orient = event.GetEventObject()
        self.orientation = enums.Orientation[orient.GetStringSelection()]


class ImagePropertiesBox(wx.FlexGridSizer):
    _image_width: Optional[int] = None
    _image_height: Optional[int] = None
    _dpi: Optional[Tuple[int, int]] = None

    def __init__(self, parent):
        self.parent = parent
        super().__init__(cols=2, rows=3, hgap=BORDER_WIDTH, vgap=BORDER_WIDTH)
        # self.SetFlexibleDirection(wx.HORIZONTAL)

        # Creates the widgets
        self.w_image_width_label = wx.StaticText(self.parent, id=wx.ID_ANY, label=_("Image width (mm)"))
        self.w_image_width = wx.TextCtrl(self.parent, id=wx.ID_ANY, style=wx.TE_READONLY)
        self.w_image_height_label = wx.StaticText(self.parent, label=_("Image height (mm)"))
        self.w_image_height = wx.TextCtrl(self.parent, style=wx.TE_READONLY)
        self.w_dpi_label = wx.StaticText(self.parent, label=_("DPI"))
        self.w_dpi = wx.TextCtrl(self.parent, style=wx.TE_READONLY)

        # Adds the widgets to the Sizer
        self.AddMany(
            [
                (self.w_image_width_label, 1, SIZER_FIELD_FLAG),
                (self.w_image_width, 1, SIZER_FIELD_FLAG),
                (self.w_image_height_label, 1, SIZER_FIELD_FLAG),
                (self.w_image_height, 1, SIZER_FIELD_FLAG),
            ]
        )
        self.Add(self.w_dpi_label, proportion=1, flag=SIZER_FIELD_FLAG)
        self.Add(self.w_dpi, proportion=1, flag=SIZER_FIELD_FLAG)

    # region Properties
    @property
    def image_width(self):
        return self._image_width

    @image_width.setter
    def image_width(self, value):
        self._image_width = value
        # self.w_image_width.value = str(value)

    @property
    def image_height(self):
        return self._image_height

    @image_height.setter
    def image_height(self, value):
        self._image_height = value
        # self.w_image_height.value = str(value)

    @property
    def dpi(self):
        return self._dpi

    @dpi.setter
    def dpi(self, value):
        if isinstance(value, int):
            self._dpi = value, value
        else:
            self._dpi = value
        self.w_dpi.SetValue(f"{self._dpi[0]}×{self._dpi[1]}")
        self.w_image_width.SetValue(str(px_to_mm(self._image_width, self._dpi[0])))
        self.w_image_height.SetValue( str(px_to_mm(self._image_height, self._dpi[1])))
        self.parent.Refresh()
    # endregion Properties
