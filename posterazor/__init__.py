"""

"""
import base64
import logging
from copy import deepcopy
from dataclasses import dataclass
from io import BytesIO
from pathlib import Path
from typing import List, Tuple, Generator, Union

from PIL.ImageQt import rgb
from jinja2 import FileSystemLoader, TemplateNotFound, Environment
from weasyprint import HTML
from PIL import Image, ImageDraw

from posterazor import enums, helpers

logger = logging.getLogger("posterazor")

settings = helpers.Settings()


@dataclass
class PaperSheet:
    paper_size: enums.PaperSize
    orientation: enums.Orientation

    dpi: Tuple[int, int]

    margin_top_mm: int
    margin_bottom_mm: int
    margin_left_mm: int
    margin_right_mm: int

    @property
    def paper_size_mm(self):
        return enums.get_width_height_from_size(self.paper_size)

    @property
    def width_mm(self):
        return self.paper_size_mm.width

    @property
    def height_mm(self):
        return self.paper_size_mm.height

    @property
    def margins(self):
        return enums.Margins(
            left=self.margin_left_mm, top=self.margin_top_mm, right=self.margin_right_mm, bottom=self.margin_bottom_mm
        )

    @margins.setter
    def margins(self, value):
        self.margin_left_mm = value.left
        self.margin_top_mm = value.top
        self.margin_right_mm = value.right
        self.margin_bottom_mm = value.bottom

    @property
    def width_px(self):
        return helpers.mm_to_px(self.width_mm, self.dpi[0])

    @property
    def height_px(self):
        return helpers.mm_to_px(self.height_mm, self.dpi[1])

    @property
    def usable_width_mm(self):
        if self.orientation == enums.Orientation.PORTRAIT:
            return self.width_mm - self.margin_left_mm - self.margin_right_mm

    @property
    def usable_height_mm(self):
        return self.height_mm - self.margin_top_mm - self.margin_bottom_mm

    @property
    def usable_frame_mm(self) -> enums.Size:
        if self.orientation == enums.Orientation.PORTRAIT:
            return enums.Size(width=self.width_mm, height=self.height_mm)
        else:
            return enums.Size(width=self.height_mm, height=self.width_mm)

    @property
    def usable_frame_px(self) -> enums.Size:
        frame = self.usable_frame_mm
        return enums.Size(
            width=helpers.mm_to_px(frame.width, self.dpi[0]),
            height=helpers.mm_to_px(frame.height, self.dpi[1]),
        )


class PosteRazor:

    # region Accessors

    @property
    def input_file(self):
        return self._input_file

    @input_file.setter
    def input_file(self, value):
        self._input_file = value

    @property
    def output_file(self):
        return self._output_file

    @output_file.setter
    def output_file(self, value):
        self._output_file = value

    @property
    def cover_side(self) -> enums.CoverSide:
        return self._cover_side

    @cover_side.setter
    def cover_side(self, value: enums.CoverSide):
        self._cover_side = value

    @property
    def cover_width(self) -> int:
        return self._cover_width

    @cover_width.setter
    def cover_width(self, value: int):
        self._cover_width = value

    @property
    def paper_sheet(self) -> PaperSheet:
        return self._paper_sheet

    @paper_sheet.setter
    def paper_sheet(self, value: PaperSheet):
        self._paper_sheet = value

    # endregion Accessors

    def __init__(self, input_file=None, output_file=None, **kwargs):
        # Files
        self._input_file: Union[Path, str, Image.Image] = input_file
        self._output_file: Union[Path, str] = output_file
        # PDF Paper
        self._cover_side: enums.CoverSide = kwargs.get("cover_side", enums.CoverSide.TOP_LEFT)
        self._cover_width: int = kwargs.get("cover_width", settings["cover_width"])

        _paper_size_model = kwargs.get("paper_size", settings["paper_size"])
        _paper_size: enums.Size = enums.get_width_height_from_size(_paper_size_model)
        _orientation_str = kwargs.get("orientation", settings["orientation"])
        if not isinstance(_orientation_str, enums.Orientation):
            _orientation: enums.Orientation = enums.Orientation[_orientation_str]
        else:
            _orientation = _orientation_str
        _dpi: int = int(kwargs.get("dpi", settings["dpi"]))
        # Margins
        _margin_top = kwargs.get("margin_top", settings["margin_top"])
        _margin_bottom = kwargs.get("margin_top", settings["margin_bottom"])
        _margin_left = kwargs.get("margin_left", settings["margin_left"])
        _margin_right = kwargs.get("margin_right", settings["margin_right"])

        self._paper_sheet = PaperSheet(
            paper_size=_paper_size_model,
            orientation=_orientation,
            dpi=(_dpi, _dpi),
            margin_top_mm=_margin_top,
            margin_bottom_mm=_margin_bottom,
            margin_left_mm=_margin_left,
            margin_right_mm=_margin_right,
        )

    def get_cols_rows_sizes(self, image):
        logger.debug(f"PosteRazor::get_cols_rows_sizes() image = {image} / size = {image.size} / infos = {image.info}")
        if "dpi" in image.info:
            self.paper_sheet.dpi = image.info["dpi"]

        frame_mm = self.paper_sheet.usable_frame_mm
        image_width_px, image_height_px = image.size[0], image.size[1]

        image_width_mm, image_height_mm = (
            helpers.px_to_mm(image_width_px, dpi=self.paper_sheet.dpi[0]),
            helpers.px_to_mm(image_height_px, dpi=self.paper_sheet.dpi[1]),
        )

        cols, cols_rest = image_width_mm // frame_mm.width, image_width_mm % frame_mm.width
        rows, rows_rest = image_height_mm // frame_mm.height, image_height_mm % frame_mm.height
        if cols_rest > 0:
            cols += 1
        if rows_rest > 0:
            rows += 1

        logger.debug(f"PosteRazor::get_cols() cols = {cols} × rows = {rows} / Total = {cols * rows}")

        frame_px = self.paper_sheet.usable_frame_px
        return (
            cols,
            rows,
            enums.Size(width=image_width_px, height=image_height_px),
            enums.Size(width=frame_px.width, height=frame_px.height),
        )

    def split_image(self) -> List[Tuple[int, int, Image.Image]]:
        if not isinstance(self._input_file, Image.Image):
            image = Image.open(self._input_file)
        else:
            image = self._input_file
        cropped_images = []
        cols, rows, _image_size_px, _frame_size_px = self.get_cols_rows_sizes(image)

        for col_id in range(cols):
            _left = _frame_size_px.width * col_id
            _right = min(_frame_size_px.width * (col_id + 1), _image_size_px.width)

            for row_id in range(rows):
                _upper = _frame_size_px.height * row_id
                _lower = min(_frame_size_px.height * (row_id + 1), _image_size_px.hight)

                logger.debug(
                    f"PosteRazor::split_image() ({row_id}x{col_id}) / _left = {_left} / _upper = {_upper} / "
                    f"_right = {_right} / _lower = {_lower}"
                )
                cropped_image = image.crop(box=(_left, _upper, _right, _lower))
                if cropped_image.size[0] == 0 or cropped_image.size[1] == 0:
                    raise ValueError
                cropped_images.append((col_id, row_id, cropped_image))
        return cropped_images

    def save_images(self, cropped_images: List[Tuple[int, int, Image.Image]]) -> int:  # noqa
        count_saved = 0
        for idx, (col_id, row_id, cropped_image) in enumerate(cropped_images):
            _w, _h = cropped_image.size
            if _w == 0 or _h == 0:
                logger.warning(f"PosteRazor::save_images() Image to small: {_w}x{_h} for {row_id}x{col_id}")
                continue
            path = Path(__file__).parents[1] / "tmp" / f"foo-{col_id:02d}-{row_id:02d}.png"
            logger.debug(f"PosteRazor::save_images() path = {path}")
            try:
                cropped_image.save(path.absolute())
                count_saved += 1
            except SystemError:
                logger.error(
                    f"PosteRazor::save_imagess() Unable to save file for path {path} / Image size: {cropped_image.size}"
                )
        return count_saved

    def get_preview(
            self, paper_size: enums.PaperSize, orientation: enums.Orientation, margins: enums.Margins
    ) -> Image.Image:
        if not isinstance(self._input_file, Image.Image):
            image = Image.open(self._input_file)
        else:
            image = self._input_file
        logger.debug(
            f"PosteRazor::get_preview() paper_size = {paper_size} / orientation = {orientation.name} / margins = {margins}")
        self.paper_sheet.paper_size = paper_size
        self.paper_sheet.orientation = orientation
        self.paper_sheet.margins = margins

        cols, rows, _image_size_px, _frame_size_px = self.get_cols_rows_sizes(image)
        draw = ImageDraw.Draw(image)
        for col in range(cols):
            if col * _frame_size_px.width <= _image_size_px.width:
                draw.line(
                    (col * _frame_size_px.width, 0, col * _frame_size_px.width, image.height),
                    fill="#f00"
                )
        for row in range(rows):
            if row * _frame_size_px.height <= _image_size_px.height:
                draw.line(
                    (0, row * _frame_size_px.height, image.width, row * _frame_size_px.height),
                    fill="#f00"
                )
        return image

    @staticmethod
    def encode_images(cropped_images) -> Generator[str, None, None]:
        for _, __, image in cropped_images:
            buffer = BytesIO()
            image.save(buffer, format="PNG")
            yield base64.b64encode(buffer.getvalue()).decode("utf-8")

    def save_pdf(self, cropped_images: List[Tuple[int, int, Image.Image]]) -> int:
        pages_count = 0
        loader = FileSystemLoader(Path(__file__).parent / "templates")
        environment = Environment(loader=loader)
        template = environment.get_template("index.html.j2")
        html_content = template.render(
            page_size=self.paper_sheet.paper_size,
            orientation=self.paper_sheet.orientation.name,
            margin_top=self.paper_sheet.margin_top_mm,
            margin_bottom=self.paper_sheet.margin_bottom_mm,
            margin_left=self.paper_sheet.margin_left_mm,
            margin_right=self.paper_sheet.margin_right_mm,
            images=[b64image for b64image in self.encode_images(cropped_images)],
        )
        html = HTML(string=html_content)
        # pdf = html.write_pdf(self.output_file)
        pdf = html.write_pdf(Path(__file__).parents[1] / "tmp" / "test.pdf")
        return pages_count
