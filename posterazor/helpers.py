"""
Helpers for the :mod:`posterazor` application
"""
import logging
from logging.config import fileConfig
import configparser
from pathlib import Path
from typing import Optional, Any

DEFAULT_SETTINGS_FILE = Path(__file__).parent / "settings.ini"
DEFAULT_LOGGING_FILE = Path(__file__).parent / "logging.ini"

logger = logging.getLogger(__name__)


class Settings:
    def __init__(self, input_file: Optional[Path] = None):
        self._parser = configparser.ConfigParser()
        self._parser.read(input_file or DEFAULT_SETTINGS_FILE)
        assert self._parser.sections() is not None
        logger.debug("Settings() Initialized...")

    def __getitem__(self, item) -> Any:
        value = self._parser["DEFAULT"][item]
        logger.debug(f"Settings['{item}'] value = '{value}'")
        return value


def init_logging():
    fileConfig(DEFAULT_LOGGING_FILE.absolute(), disable_existing_loggers=True)


def px_to_mm(dim: int, dpi: int) -> int:
    return round(dim / (dpi / 25.4))


def mm_to_px(dim: int, dpi: int) -> int:
    return round(dim * (dpi / 25.4))