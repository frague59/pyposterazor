"""
Enums for the :mod:`posterazor` application
"""
import logging
import enum
from collections import namedtuple
from typing import Union

logger = logging.getLogger(__name__)


Size = namedtuple("Size", ("width", "height"))
Margins = namedtuple("Margins", ("left", "top", "right", "bottom"))


class CoverSide(enum.Enum):
    TOP_LEFT = "TOP_LEFT"
    TOP_RIGHT = "TOP_RIGHT"
    BOTTOM_LEFT = "BOTTOM_LEFT"
    BOTTOM_RIGHT = "BOTTOM_RIGHT"


class PaperSize(enum.Enum):
    A4 = "210×297"
    A3 = "297×420"
    A2 = "420×594"
    A1 = "594×840"
    A0 = "840×1189"
    US_Letter = "279×216"
    US_Legal = "356×216"
    US_Ledger = "432×279"


class Orientation(enum.Enum):
    PORTRAIT = "PORTRAIT"
    LANDSCAPE = "LANDSCAPE"


def get_width_height_from_size(paper_size: Union[PaperSize, str]) -> Size:
    logger.debug(f"get_width_height_from_size() paper_size = {paper_size}")
    if isinstance(paper_size, str):
        paper_size = PaperSize[paper_size]
    width, height = paper_size.value.split("×")
    output = Size(width=int(width), height=int(height))
    return output
