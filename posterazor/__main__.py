#!/usr/bin/env python3
"""
Entry point for the CLI app to posterize some images
"""
import sys
import click

from .helpers import Settings, init_logging
from . import PosteRazor
from . import enums
from . import gui

# Initialize the logging


settings: Settings = Settings()
init_logging()


@click.command()
@click.option("-i", "--input-file", type=click.File("rb"), help="Image input file to convert into a sliced PDF file")
@click.option("-o", "--output-file", type=click.File("wb"), help="Output PDF File the images for slices")
@click.option(
    "-p",
    "--paper-size",
    default=settings["paper_size"],
    type=click.Choice([name for name, _ in enums.PaperSize.__members__.items()], case_sensitive=False),
    help="Paper size",
)
@click.option(
    "-o",
    "--orientation",
    default=settings["orientation"],
    type=click.Choice([name for name, _ in enums.Orientation.__members__.items()], case_sensitive=False),
    help="Paper orientation",
)
@click.option("-g", "--gui", "start_gui", default=False, type=bool, help="Start GUI", is_flag=True)
def main(
    output_file=None, input_file=None, paper_size: str = None, orientation: str = None, start_gui: bool = False
) -> int:
    if start_gui:
        _gui = gui.AppWindow()
        _gui.start()
        return 0
    else:
        posterazor = PosteRazor(
            input_file=input_file,
            output_file=output_file,
            paper_size=paper_size,
            orientation=orientation,
        )
        cropped_images = posterazor.split_image()
        posterazor.save_images(cropped_images=cropped_images)
        posterazor.save_pdf(cropped_images=cropped_images)
    return 0


if __name__ == "__main__":
    sys.exit(main(sys.argv[1:]))
