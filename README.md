# PyPosteRazor

A slicer to cut large drawings into printable PDF file.

This project is a re-implementation of the well-known [PosteRazor](https://posterazor.sourceforge.io/) application, written in python. 
It uses Pillow as image manipulation library, weasyprint for PDF generation and wxPython for GUI.
 